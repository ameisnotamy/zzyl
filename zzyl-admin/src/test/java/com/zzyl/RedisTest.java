package com.zzyl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Test
    public void test(){
        // 打印RedisTemplate对象
        System.out.println(redisTemplate);

    }



    @Test
    public void testString(){
        /*
        redisTemplate.opsForValue().set("name", "zzyl");
        System.out.println(redisTemplate.opsForValue().get("name"));
        */

        redisTemplate.opsForValue().set("age", "18", 30, TimeUnit.SECONDS);

        Boolean age = redisTemplate.opsForValue().setIfAbsent("age", "19");
        System.out.println(age);
    }

    @Test
    public void testHash(){
        redisTemplate.opsForHash().put("user", "name", "zzyl");
        redisTemplate.opsForHash().put("user", "age", "18");
        System.out.println(redisTemplate.opsForHash().get("user", "name"));
        System.out.println(redisTemplate.opsForHash().values("user"));
        System.out.println(redisTemplate.opsForHash().keys("user"));
    }

    @Test
    public void testList(){
        //lpush lrange rpop lpop llen
        redisTemplate.opsForList().leftPushAll("list", "a", "b", "c");

        List<String> list = redisTemplate.opsForList().range("list", 0, -1);
        System.out.println(list);

        System.out.println(redisTemplate.opsForList().rightPop("list"));
        System.out.println(redisTemplate.opsForList().leftPop("list"));

        System.out.println(redisTemplate.opsForList().size("list"));

    }

    @Test
    public void testSet(){
        // sadd smembers scard sinter sunion
        redisTemplate.opsForSet().add("myset", "a", "b", "c");
        redisTemplate.opsForSet().add("myset2", "a", "c", "d");

        Set<String> myset = redisTemplate.opsForSet().members("myset");
        System.out.println(myset);

        Long myset1 = redisTemplate.opsForSet().size("myset");
        System.out.println(myset1);

        Set<String> intersect = redisTemplate.opsForSet().intersect("myset", "myset2");
        System.out.println(intersect);
        Set<String> union = redisTemplate.opsForSet().union("myset", "myset2");
        System.out.println(union);
    }

    @Test
    public void testZSet(){
        //zadd zrange zincrby zrem
        redisTemplate.opsForZSet().add("zset", "a", 1);
        redisTemplate.opsForZSet().add("zset", "b", 2);
        redisTemplate.opsForZSet().add("zset", "c", 3);

        Set<String> zset = redisTemplate.opsForZSet().range("zset", 0, -1);
        System.out.println(zset);

        redisTemplate.opsForZSet().incrementScore("zset", "a", 10);
        System.out.println(redisTemplate.opsForZSet().range("zset", 0, -1));

        redisTemplate.opsForZSet().remove("zset", "a");
        System.out.println(redisTemplate.opsForZSet().range("zset", 0, -1));
    }

    @Test
    public void testCommon(){
        //keys exists type del
        System.out.println(redisTemplate.keys("*"));

        System.out.println(redisTemplate.hasKey("list"));
        System.out.println(redisTemplate.hasKey("zset"));

        System.out.println(redisTemplate.type("zset"));

        redisTemplate.delete("zset");
    }
}
