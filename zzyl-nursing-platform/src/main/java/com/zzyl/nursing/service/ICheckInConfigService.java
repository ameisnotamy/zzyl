package com.zzyl.nursing.service;

import java.util.List;
import com.zzyl.nursing.domain.CheckInConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 配置Service接口
 * 
 * @author ruoyi
 * @date 2024-09-13
 */
public interface ICheckInConfigService extends IService<CheckInConfig>
{
    /**
     * 查询配置
     * 
     * @param id 配置主键
     * @return 配置
     */
    public CheckInConfig selectCheckInConfigById(Long id);

    /**
     * 查询配置列表
     * 
     * @param checkInConfig 配置
     * @return 配置集合
     */
    public List<CheckInConfig> selectCheckInConfigList(CheckInConfig checkInConfig);

    /**
     * 新增配置
     * 
     * @param checkInConfig 配置
     * @return 结果
     */
    public int insertCheckInConfig(CheckInConfig checkInConfig);

    /**
     * 修改配置
     * 
     * @param checkInConfig 配置
     * @return 结果
     */
    public int updateCheckInConfig(CheckInConfig checkInConfig);

    /**
     * 批量删除配置
     * 
     * @param ids 需要删除的配置主键集合
     * @return 结果
     */
    public int deleteCheckInConfigByIds(Long[] ids);

    /**
     * 删除配置信息
     * 
     * @param id 配置主键
     * @return 结果
     */
    public int deleteCheckInConfigById(Long id);
}
