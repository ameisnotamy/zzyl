package com.zzyl.nursing.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zzyl.common.exception.base.BaseException;
import com.zzyl.common.utils.DateUtils;
import com.zzyl.nursing.domain.*;
import com.zzyl.nursing.dto.CheckInApplyDto;
import com.zzyl.nursing.dto.CheckInConfigDto;
import com.zzyl.nursing.mapper.*;
import com.zzyl.nursing.util.CodeGenerator;
import com.zzyl.nursing.vo.CheckInConfigVo;
import com.zzyl.nursing.vo.CheckInDetailVo;
import com.zzyl.nursing.vo.CheckInElderVo;
import com.zzyl.nursing.vo.ElderFamilyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zzyl.nursing.service.ICheckInService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
/**
 * 入住Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-09-13
 */
@Service
public class CheckInServiceImpl extends ServiceImpl<CheckInMapper,CheckIn> implements ICheckInService
{
    @Autowired
    private CheckInMapper checkInMapper;
    @Autowired
    private ElderMapper elderMapper;
    @Autowired
    private BedMapper bedMapper;
    @Autowired
    private CheckInConfigMapper checkInConfigMapper;
    @Autowired
    private ContractMapper contractMapper;

    /**
     * 查询入住
     * 
     * @param id 入住主键
     * @return 入住
     */
    @Override
    public CheckIn selectCheckInById(Long id)
    {
        return getById(id);
    }

    /**
     * 查询入住列表
     * 
     * @param checkIn 入住
     * @return 入住
     */
    @Override
    public List<CheckIn> selectCheckInList(CheckIn checkIn)
    {
        return checkInMapper.selectCheckInList(checkIn);
    }

    /**
     * 新增入住
     * 
     * @param checkIn 入住
     * @return 结果
     */
    @Override
    public int insertCheckIn(CheckIn checkIn)
    {
        return save(checkIn)?1:0;
    }

    /**
     * 修改入住
     * 
     * @param checkIn 入住
     * @return 结果
     */
    @Override
    public int updateCheckIn(CheckIn checkIn)
    {
        return updateById(checkIn)?1:0;
    }

    /**
     * 批量删除入住
     * 
     * @param ids 需要删除的入住主键
     * @return 结果
     */
    @Override
    public int deleteCheckInByIds(Long[] ids)
    {
        return removeByIds(Arrays.asList(ids))?1:0;
    }

    /**
     * 删除入住信息
     * 
     * @param id 入住主键
     * @return 结果
     */
    @Override
    public int deleteCheckInById(Long id)
    {
        return removeById(id)?1:0;
    }



    /**
     * 入住申请
     *
     * @param checkInApplyDto 入住表单
     */
    @Override
    @Transactional
    public void apply(CheckInApplyDto checkInApplyDto) {
        try {
            // 先判断老人是否已经入住
            //通过身份证查询老人
            String idCardNo = checkInApplyDto.getCheckInElderDto().getIdCardNo();
            Elder elder = elderMapper.selectOne(
                    new LambdaQueryWrapper<Elder>()
                    .eq(Elder::getIdCardNo, idCardNo)
                    .eq(Elder::getStatus, 1)
            );
            if(ObjectUtil.isNotEmpty(elder)){
                throw new BaseException("老人已入住");
            }

            //未入住，开始保存数据
            // 先将床位状态改为已入住
            Bed bed = bedMapper.selectById(checkInApplyDto.getCheckInConfigDto().getBedId());
            bed.setBedStatus(1);
            bedMapper.updateById(bed);

            // 开始保存老人数据
            elder = saveElder(checkInApplyDto, bed);

            //生成合同编号
            String contractNo = "HT" + CodeGenerator.generateContractNumber();

            // 保存入住数据
            CheckIn checkIn = saveCheckIn(checkInApplyDto, elder);

            // 保存合同数据
            insertContract(checkInApplyDto, elder, contractNo);

            // 保存入住配置数据
            insertCheckInConfig(checkInApplyDto, checkIn.getId());
        } catch (BaseException e) {
            throw new BaseException("入住失败");
        }
    }

    private void insertCheckInConfig(CheckInApplyDto checkInApplyDto, Long id) {
        CheckInConfig checkInConfig = new CheckInConfig();
        BeanUtil.copyProperties(checkInApplyDto.getCheckInConfigDto(), checkInConfig);
        checkInConfig.setCheckInId(id);
        checkInConfigMapper.insert(checkInConfig);
    }

    private void insertContract(CheckInApplyDto checkInApplyDto, Elder elder, String contractNo) {
        Contract contract = new Contract();
        BeanUtil.copyProperties(checkInApplyDto.getCheckInContractDto(), contract);
        contract.setContractNumber(contractNo);
        contract.setElderId(elder.getId());
        contract.setElderName(elder.getName());
        LocalDateTime checkInStartTime = checkInApplyDto.getCheckInConfigDto().getStartDate();
        LocalDateTime checkInEndTime = checkInApplyDto.getCheckInConfigDto().getEndDate();
        Integer status = checkInStartTime.isAfter(LocalDateTime.now()) ? 1 : 0;
        contract.setStatus(status);
        contract.setStartDate(checkInStartTime);
        contract.setEndDate(checkInEndTime);
        contractMapper.insert(contract);
    }

    private CheckIn saveCheckIn(CheckInApplyDto checkInApplyDto, Elder elder) {
        CheckIn checkIn = new CheckIn();
        BeanUtil.copyProperties(checkInApplyDto.getCheckInConfigDto(), checkIn);
        checkIn.setElderId(elder.getId());
        checkIn.setElderName(elder.getName());
        checkIn.setIdCardNo(elder.getIdCardNo());
        checkIn.setBedNumber(elder.getBedNumber());
        checkIn.setStatus(0);
        checkIn.setRemark(JSONUtil.toJsonStr(checkInApplyDto.getElderFamilyDtoList()));
        checkInMapper.insert(checkIn);
        return checkIn;
    }

    private Elder saveElder(CheckInApplyDto checkInApplyDto , Bed bed) {
        Elder elder = new Elder();
        BeanUtil.copyProperties(checkInApplyDto.getCheckInElderDto(), elder);
        elder.setBedId(bed.getId());
        elder.setBedNumber(bed.getBedNumber());
        elder.setStatus(1);
        // 查询是修改还是新增
        Elder elderDb = elderMapper.selectOne(
                new LambdaQueryWrapper<Elder>()
                        .eq(Elder::getIdCardNo, elder.getIdCardNo())
                        .eq(Elder::getStatus, 0)
        );
        if(ObjectUtil.isNotEmpty(elderDb)){
            // 修改
            elder.setId(elderDb.getId());
            elderMapper.updateById(elder);
        }else{
            // 新增
            elderMapper.insert(elder);
        }
        return elder;
    }

    /**
     * 获取入住详情
     *
     * @param id 入住ID
     * @return CheckInDetailVo
     */
    @Override
    public CheckInDetailVo getDetailById(Long id) {
        CheckInDetailVo checkInDetailVo = new CheckInDetailVo();
        // 先获取入住信息
        CheckIn checkIn = checkInMapper.selectOne(
                new LambdaQueryWrapper<CheckIn>()
                        .eq(CheckIn::getId, id)
                        .eq(CheckIn::getStatus, 0)
        );
        // 获取老人信息
        checkInDetailVo.setCheckInElderVo(getElderInfo(checkIn));
        // 获取家属信息
        List<ElderFamilyVo> elderFamilyVoList = JSONUtil.toList(checkIn.getRemark(), ElderFamilyVo.class);
        checkInDetailVo.setElderFamilyVoList(elderFamilyVoList);
        // 获取入住配置信息
        checkInDetailVo.setCheckInConfigVo(getCheckinConfigVo(checkIn));
        // 获取合同信息
        checkInDetailVo.setContract(
                contractMapper.selectOne(
                        new LambdaQueryWrapper<Contract>()
                                .eq(Contract::getElderId, checkIn.getElderId())
                                .eq(Contract::getStatus, 1)
                )
        );

        // 返回
        return checkInDetailVo;
    }

    private CheckInConfigVo getCheckinConfigVo(CheckIn checkIn) {
        CheckInConfigVo checkInConfigVo = new CheckInConfigVo();

        CheckInConfig checkInConfig = checkInConfigMapper.selectOne(
                new LambdaQueryWrapper<CheckInConfig>()
                        .eq(CheckInConfig::getCheckInId, checkIn.getId())
        );
        if (ObjectUtil.isNotEmpty(checkInConfig)) {
            BeanUtil.copyProperties(checkInConfig, checkInConfigVo);
            checkInConfigVo.setStartDate(checkIn.getStartDate());
            checkInConfigVo.setEndDate(checkIn.getEndDate());
            checkInConfigVo.setBedNumber(checkIn.getBedNumber());
        }
        return checkInConfigVo;
    }

    /**
     * 获取入住老人信息
     * @param checkIn 入住信息
     * @return CheckInElderVo
     */
    private CheckInElderVo getElderInfo(CheckIn checkIn) {
        CheckInElderVo checkInElderVo = new CheckInElderVo();
        // 根据老人id查询老人信息
        Elder elder = elderMapper.selectOne(
                new LambdaQueryWrapper<Elder>()
                        .eq(Elder::getId, checkIn.getElderId())
                        .eq(Elder::getStatus, 1)
        );
        if (ObjectUtil.isNotEmpty(elder)) {
            BeanUtil.copyProperties(elder, checkInElderVo);
            // 根据出生日期计算年龄
            checkInElderVo.setAge(DateUtil.ageOfNow(elder.getBirthday()));
        }
        return checkInElderVo;
    }
}
