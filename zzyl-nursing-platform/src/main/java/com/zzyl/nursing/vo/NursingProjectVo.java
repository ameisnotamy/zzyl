package com.zzyl.nursing.vo;

import lombok.Data;

@Data
public class NursingProjectVo {

    /**
     * 名称
     */
    private String label;

    /**
     * 值（ID）
     */
    private String value;

}
