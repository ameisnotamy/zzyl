package com.zzyl.nursing.mapper;

import java.util.List;
import com.zzyl.nursing.domain.CheckInConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 配置Mapper接口
 * 
 * @author ruoyi
 * @date 2024-09-13
 */
@Mapper
public interface CheckInConfigMapper extends BaseMapper<CheckInConfig>
{
    /**
     * 查询配置
     * 
     * @param id 配置主键
     * @return 配置
     */
    public CheckInConfig selectCheckInConfigById(Long id);

    /**
     * 查询配置列表
     * 
     * @param checkInConfig 配置
     * @return 配置集合
     */
    public List<CheckInConfig> selectCheckInConfigList(CheckInConfig checkInConfig);

    /**
     * 新增配置
     * 
     * @param checkInConfig 配置
     * @return 结果
     */
    public int insertCheckInConfig(CheckInConfig checkInConfig);

    /**
     * 修改配置
     * 
     * @param checkInConfig 配置
     * @return 结果
     */
    public int updateCheckInConfig(CheckInConfig checkInConfig);

    /**
     * 删除配置
     * 
     * @param id 配置主键
     * @return 结果
     */
    public int deleteCheckInConfigById(Long id);

    /**
     * 批量删除配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCheckInConfigByIds(Long[] ids);
}
