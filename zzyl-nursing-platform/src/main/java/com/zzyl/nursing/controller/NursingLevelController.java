package com.zzyl.nursing.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.zzyl.nursing.vo.NursingLevelVo;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.zzyl.common.annotation.Log;
import com.zzyl.common.core.controller.BaseController;
import com.zzyl.common.core.domain.AjaxResult;
import com.zzyl.common.enums.BusinessType;
import com.zzyl.nursing.domain.NursingLevel;
import com.zzyl.nursing.service.INursingLevelService;
import com.zzyl.common.utils.poi.ExcelUtil;
import com.zzyl.common.core.page.TableDataInfo;

/**
 * 护理等级Controller
 * 
 * @author ruoyi
 * @date 2024-09-07
 */
@RestController
@RequestMapping("/nursing/nursingLevel")
public class NursingLevelController extends BaseController
{
    @Autowired
    private INursingLevelService nursingLevelService;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    private static final String CACHE_KEY = "nursingLevel:all";

    /**
     * 查询护理等级列表
     */
    @PreAuthorize("@ss.hasPermi('nursing:nursingLevel:list')")
    @GetMapping("/list")
    public TableDataInfo list(NursingLevel nursingLevel)
    {
        startPage();
        List<NursingLevelVo> list = nursingLevelService.selectNursingLevelList(nursingLevel);
        return getDataTable(list);
    }

    /**
     * 获取护理等级详细信息
     */
    @PreAuthorize("@ss.hasPermi('nursing:nursingLevel:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(nursingLevelService.selectNursingLevelById(id));
    }

    /**
     * 新增护理等级
     */
    @PreAuthorize("@ss.hasPermi('nursing:nursingLevel:add')")
    @Log(title = "护理等级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NursingLevel nursingLevel)
    {
        int result = nursingLevelService.insertNursingLevel(nursingLevel);
        deleteCache();
        return toAjax(result);
    }

    /**
     * 修改护理等级
     */
    @PreAuthorize("@ss.hasPermi('nursing:nursingLevel:edit')")
    @Log(title = "护理等级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NursingLevel nursingLevel)
    {
        int result = nursingLevelService.updateNursingLevel(nursingLevel);
        deleteCache();
        return toAjax(result);
    }

    /**
     * 删除护理等级
     */
    @PreAuthorize("@ss.hasPermi('nursing:nursingLevel:remove')")
    @Log(title = "护理等级", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        int result = nursingLevelService.deleteNursingLevelByIds(ids);
        deleteCache();
        return toAjax(result);
    }

    /**
     * 查询护理等级列表
     * @return
     */
    @GetMapping("/listAll")
    public AjaxResult listAll(){
        // 先从redis中获取
        List<NursingLevel> nursingLevels = (List<NursingLevel>) redisTemplate.opsForValue().get(CACHE_KEY);
        // 如果缓存中有，直接返回
        if (nursingLevels != null && !nursingLevels.isEmpty()){
            return success(nursingLevels);
        }
        // 缓存中没有，从数据库中查询
        nursingLevels = nursingLevelService.listAll();
        // 设置缓存
        redisTemplate.opsForValue().set(CACHE_KEY, nursingLevels);
        return success(nursingLevels);
    }

    /**
     * 删除缓存
     */
    private void deleteCache() {
        redisTemplate.delete(CACHE_KEY);
    }
}
