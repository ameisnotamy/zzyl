package com.zzyl.nursing.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.zzyl.nursing.vo.NursingProjectVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.zzyl.common.annotation.Log;
import com.zzyl.common.core.controller.BaseController;
import com.zzyl.common.core.domain.AjaxResult;
import com.zzyl.common.enums.BusinessType;
import com.zzyl.nursing.domain.NursingProject;
import com.zzyl.nursing.service.INursingProjectService;
import com.zzyl.common.utils.poi.ExcelUtil;
import com.zzyl.common.core.page.TableDataInfo;

/**
 * 护理项目Controller
 * 
 * @author ruoyi
 * @date 2024-09-12
 */
@RestController
@RequestMapping("/serve/project")
@Api(tags = "护理项目的接口")
public class NursingProjectController extends BaseController
{
    @Autowired
    private INursingProjectService nursingProjectService;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;
    private static final String CACHE_KEY = "nursingProject:all";

    @GetMapping("/all")
    public AjaxResult listAll(){
        // 先从redis中获取
        List<NursingProjectVo> nursingProjectList = (List<NursingProjectVo>) redisTemplate.opsForValue().get(CACHE_KEY);
        if (nursingProjectList != null && !nursingProjectList.isEmpty()){
            return success(nursingProjectList);
        }
        // 缓存中没有，从数据库中查询
        nursingProjectList = nursingProjectService.listAll();
        // 设置缓存
        redisTemplate.opsForValue().set(CACHE_KEY, nursingProjectList);
        return success(nursingProjectList);
    }

    /**
     * 查询护理项目列表
     */
    @ApiOperation("查询护理项目列表")
//    @PreAuthorize("@ss.hasPermi('nursing:project:list')")
    @GetMapping("/list")
    public TableDataInfo list(NursingProject nursingProject)
    {
        startPage();
        List<NursingProject> list = nursingProjectService.selectNursingProjectList(nursingProject);
        return getDataTable(list);
    }

    /**
     * 导出护理项目列表
     */
    @ApiOperation("导出护理项目列表")
//    @PreAuthorize("@ss.hasPermi('nursing:project:export')")
    @Log(title = "护理项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NursingProject nursingProject)
    {
        List<NursingProject> list = nursingProjectService.selectNursingProjectList(nursingProject);
        ExcelUtil<NursingProject> util = new ExcelUtil<NursingProject>(NursingProject.class);
        util.exportExcel(response, list, "护理项目数据");
    }

    /**
     * 获取护理项目详细信息
     */
    @ApiOperation("获取护理项目详细信息")
//    @PreAuthorize("@ss.hasPermi('nursing:project:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@ApiParam(value = "护理项目ID", required = true)
            @PathVariable("id") Long id)
    {
        return success(nursingProjectService.selectNursingProjectById(id));
    }

    /**
     * 新增护理项目
     */
    @ApiOperation("新增护理项目")
//    @PreAuthorize("@ss.hasPermi('nursing:project:add')")
    @Log(title = "护理项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@ApiParam(value = "护理项目实体")
            @RequestBody NursingProject nursingProject)
    {
        int result = nursingProjectService.insertNursingProject(nursingProject);
        deleteCache();
        return toAjax(result);
    }

    /**
     * 修改护理项目
     */
    @ApiOperation("修改护理项目")
//    @PreAuthorize("@ss.hasPermi('nursing:project:edit')")
    @Log(title = "护理项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@ApiParam(value = "护理项目实体")
            @RequestBody NursingProject nursingProject)
    {
        int result = nursingProjectService.updateNursingProject(nursingProject);
        deleteCache();
        return toAjax(result);
    }

    /**
     * 删除护理项目
     */
    @ApiOperation("删除护理项目")
//    @PreAuthorize("@ss.hasPermi('nursing:project:remove')")
    @Log(title = "护理项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        int result = nursingProjectService.deleteNursingProjectByIds(ids);
        deleteCache();
        return toAjax(result);
    }

    /**
     * 清空缓存
     */
    public void deleteCache()
    {
        redisTemplate.delete(CACHE_KEY);
    }
}
