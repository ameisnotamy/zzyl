package com.zzyl;

import com.baidubce.qianfan.Qianfan;
import com.baidubce.qianfan.core.auth.Auth;
import com.baidubce.qianfan.model.chat.ChatResponse;

public class QianFanTest {
    public static void main(String[] args) {
        Qianfan qianfan = new Qianfan(Auth.TYPE_OAUTH, "aa7NkhZuA4RONSVtSywwKUew", "mMSBu0R2jXvcg4Kbn8LhMKHofs1AZIjA");
        ChatResponse response = qianfan.chatCompletion()
                .model("ERNIE-4.0-8K-Preview") // 模型名称，要选择自己开通付费的模型
                .addMessage("user", "你好") // 聊天内容，可以设置多个，每个消息包含role（角色，user表示用户，assistant表示模型），content（消息内容）
//                .temperature(0.7) // 采样参数，取值范围(0,1]
//                .maxOutputTokens(2000) // 模型输出最大长度，取值范围[2, 2048]
//                .responseFormat("json_object")  // 模型输出格式，取值范围：text（文本）、json_object（JSON对象）
                .execute();
        String result = response.getResult();
        System.out.println(result);
    }
}
